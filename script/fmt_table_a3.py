"""
Format default values for parameters from table1
"""
from pathlib import Path

import pandas as pd

from prieto2010 import pth_clean

df = pd.read_csv("../raw/table_a3.csv", sep=";", comment="#")

df['unit'] = df['unit'].apply(lambda s: f"[{s}]")

# write table
df = df.set_index(['name'])

with open(pth_clean / "table_a3.csv", 'w', encoding="utf-8") as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("# name: name of variable\n")
    fhw.write("# ref: Reference for value\n")
    fhw.write("# unit: [none]\n")
    fhw.write("# value: [unit]\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.2f")
