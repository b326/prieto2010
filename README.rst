========================
prieto2010
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/prieto2010/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/prieto2010/1.1.0/

.. image:: https://b326.gitlab.io/prieto2010/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/prieto2010

.. image:: https://b326.gitlab.io/prieto2010/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/prieto2010/

.. image:: https://badge.fury.io/py/prieto2010.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/prieto2010

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/prieto2010/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/prieto2010/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/prieto2010/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/prieto2010/commits/main
.. #}

Data and formalisms from Prieto et al. 2010 paper

