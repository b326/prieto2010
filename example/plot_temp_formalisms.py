"""
Temperature dependency
======================

Plot evolution of all parameters that depend on leaf temperature
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from prieto2010 import pth_clean, appendix

# read data
tab_a2 = pd.read_csv(pth_clean / f"table_a2.csv", sep=";", comment="#", index_col=['name'])
tab_a3 = pd.read_csv(pth_clean / f"table_a3.csv", sep=";", comment="#", index_col=['name'])
pval = pd.concat([tab_a2['value'], tab_a3['value']]).to_dict()

tab3 = pd.read_csv(pth_clean / f"table3.csv", sep=";", comment="#", index_col=['pos', 'name'])
tab3 = tab3['value'].sort_index()

tab5 = pd.read_csv(pth_clean / f"table5.csv", sep=";", comment="#", index_col=['param', 'coeff'])
coeff = tab5['value'].to_dict()

ts = np.linspace(0, 50, 100)

# compute values at 25 [°C]
na_inner = 1.2  # [g.m-2] average value for inner leaves
na_outer = 2.  # [g.m-2] average value for outer leaves

v25_inner = {}
v25_outer = {}
for name in ['vc_max', 'j_max', 'tpu', 'rd']:
    v25_inner[name] = appendix.eq9(na_inner, coeff[(f'{name}_25', 'sna')], coeff[(f'{name}_25', 'b')])
    v25_outer[name] = appendix.eq9(na_outer, coeff[(f'{name}_25', 'sna')], coeff[(f'{name}_25', 'b')])

# plot data
fig, axes = plt.subplots(2, 4, sharex='all', figsize=(10, 7), squeeze=False)

for j, (name, unit) in enumerate([('kc', "µmol.mol-1"), ('ko', "mmol.mol-1"), ('gamma_star', "µmol.mol-1")]):
    ax = axes[0, j]
    ax.set_title(name)
    ax.plot(ts, [appendix.eq7(t_leaf, pval[f'{name}_c'], pval[f'{name}_deltaha']) for t_leaf in ts])
    ax.plot([25], pval[f'{name}_25'], 'o')
    ax.set_ylabel(f"[{unit}]")

# rd is specific
name, unit = 'rd', "µmol.m-2.s-1"
ax = axes[0, -1]
ax.set_title(name)
crv, = ax.plot([24.5] * 5, tab3.loc[('inner', name)], 'o')
ax.plot(ts, [v25_inner[name] * appendix.eq7(t_leaf,
                                            pval[f'{name}_c'],
                                            pval[f'{name}_deltaha']) for t_leaf in ts],
        color=crv.get_color(), label="inner")
crv, = ax.plot([25.5] * 5, tab3.loc[('outer', name)], 'o')
ax.plot(ts, [v25_outer[name] * appendix.eq7(t_leaf,
                                            pval[f'{name}_c'],
                                            pval[f'{name}_deltaha']) for t_leaf in ts],
        color=crv.get_color(), label="outer")
ax.set_ylabel(f"[{unit}]")

for j, name in enumerate(['vc_max', 'j_max', 'tpu']):
    ax = axes[1, j]
    ax.set_title(name)
    crv, = ax.plot([24.5] * 5, tab3.loc[('inner', name)], 'o')
    ax.plot(ts, [v25_inner[name] * appendix.eq8(t_leaf,
                                                pval[f'{name}_c'],
                                                pval[f'{name}_deltaha'],
                                                pval[f'delta_hd'],
                                                pval[f'delta_s']) for t_leaf in ts],
            color=crv.get_color(), label="inner")
    crv, = ax.plot([25.5] * 5, tab3.loc[('outer', name)], 'o')
    ax.plot(ts, [v25_outer[name] * appendix.eq8(t_leaf,
                                                pval[f'{name}_c'],
                                                pval[f'{name}_deltaha'],
                                                pval[f'delta_hd'],
                                                pval[f'delta_s']) for t_leaf in ts],
            color=crv.get_color(), label="outer")

    ax.set_ylabel("[µmol.m-2.s-1]")

axes[-1, -1].set_visible(False)
for ax in axes[-1, :]:
    ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
