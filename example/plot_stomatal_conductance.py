"""
Stomatal conductance
====================

Evolution of stomatal conductance for different factors.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from prieto2010 import appendix as app, pth_clean

# read params
tab4 = pd.read_csv(pth_clean / f"table4.csv", sep=";", comment="#", index_col=['name'])
tab4 = tab4[tab4['group'] == 1]

tab_a2 = pd.read_csv(pth_clean / f"table_a2.csv", sep=";", comment="#", index_col=['name'])
tab_a3 = pd.read_csv(pth_clean / f"table_a3.csv", sep=";", comment="#", index_col=['name'])

pval = pd.concat([tab4['value'], tab_a2['value'], tab_a3['value']]).to_dict()

ca = 37  # [Pa] value around year 2000

# compute values for increasing An
vpd = 1  # [kPa] from text p1314 and fig8
t_leaf = 25  # [°C] why not!

gamma = app.eq7(t_leaf, pval[f'gamma_star_c'], pval[f'gamma_star_deltaha'])  # TODO use gamma_star for the moment
gamma *= 0.1  # [ppm] to [Pa]

records = []
for an in np.linspace(0, 15, 100):
    cs = app.eq11(an, ca, pval['gb'])
    gs = app.eq10(an, vpd, cs, gamma, pval['g0'], pval['a1'], pval['d0'])

    records.append(dict(
        an=an,
        cs=cs,
        gs=gs
    ))

df_an = pd.DataFrame(records)

# compute values for increasing VPD
t_leaf = 25  # [°C] why not!
an = 10  # [µmol.m-2.s-1]

gamma = app.eq7(t_leaf, pval[f'gamma_star_c'], pval[f'gamma_star_deltaha'])  # TODO use gamma_star for the moment
gamma *= 0.1  # [ppm] to [Pa]
cs = app.eq11(an, ca, pval['gb'])

records = []
for vpd in np.linspace(0, 3, 100):
    gs = app.eq10(an, vpd, cs, gamma, pval['g0'], pval['a1'], pval['d0'])

    records.append(dict(
        vpd=vpd,
        gs=gs
    ))

df_vpd = pd.DataFrame(records)

# compute values for increasing temperature
vpd = 1  # [kPa] from text p1314 and fig8
an = 10  # [µmol.m-2.s-1]

cs = app.eq11(an, ca, pval['gb'])

records = []
for t_leaf in np.linspace(0, 45, 100):
    gamma = app.eq7(t_leaf, pval[f'gamma_star_c'], pval[f'gamma_star_deltaha'])  # TODO use gamma_star for the moment
    gamma *= 0.1  # [ppm] to [Pa]

    gs = app.eq10(an, vpd, cs, gamma, pval['g0'], pval['a1'], pval['d0'])

    records.append(dict(
        t_leaf=t_leaf,
        gs=gs
    ))

df_temp = pd.DataFrame(records)

# plot result
fig, axes = plt.subplots(2, 3, sharex='col', sharey='row', figsize=(10, 7), squeeze=False)
ax = axes[0, 0]
ax.plot(df_an['an'], df_an['cs'])
ax.axhline(y=ca, ls='--', color='#aaaaaa')
ax.text(14, ca, "Ca", ha='right', va='top')
ax.axhline(y=gamma, ls='--', color='#aaaaaa')
ax.text(14, gamma, "gamma_star", ha='right', va='bottom')
ax.set_ylabel("Cs [Pa]")

ax = axes[1, 0]
ax.plot(df_an['an'], df_an['gs'])
ax.set_xlabel("An [µmol.m-2.s-1]")
ax.set_ylabel("gs [mmol H2O.m-2.s-1]")

axes[0, 1].set_visible(False)
ax = axes[1, 1]
ax.plot(df_vpd['vpd'], df_vpd['gs'])
ax.set_xlabel("VPD [kPa]")

axes[0, 2].set_visible(False)
ax = axes[1, 2]
ax.plot(df_temp['t_leaf'], df_temp['gs'])
ax.set_xlabel("t_leaf [°C]")

fig.tight_layout()
plt.show()
