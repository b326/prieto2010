"""
Fig7 (nitrogen content)
=======================

Plot regression using coefficient in table5 to compare with fig7
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from prieto2010 import pth_clean

# read data
tab5 = pd.read_csv(pth_clean / f"table5.csv", sep=";", comment="#", index_col=['param', 'coeff'])
reg = tab5['value'].to_dict()

nas = np.linspace(0, 3, 10)

# plot data
fig, axes = plt.subplots(2, 2, sharex='all', figsize=(8, 6), squeeze=False)

for ax, (name, ymax, step) in zip(axes.flatten(), [('vc_max', 140, 20),
                                                   ('tpu', 18, 3),
                                                   ('j_max', 250, 50),
                                                   ('rd', 1.5, 0.2)]):
    ax.plot(nas, reg[(f'{name}_25', 'sna')] * nas + reg[(f'{name}_25', 'b')])

    ax.set_ylim(0, ymax)
    ax.set_ylabel(f"{name} [µmol.m-2.s-1]")
    ax.set_yticks(np.arange(0, ymax, step))

for ax in axes[-1, :]:
    ax.set_xlim(0, 3)
    ax.set_xlabel("Na [g.m-2]")

fig.tight_layout()
plt.show()
