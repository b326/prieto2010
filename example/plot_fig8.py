"""
Fig8 (leaf gas exchange)
========================

Plot formatted data to compare with fig8
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from prieto2010 import appendix as app, pth_clean, raw

# read params
tab4 = pd.read_csv(pth_clean / f"table4.csv", sep=";", comment="#", index_col=['name'])
tab4 = tab4[tab4['group'] == 1]
tab5 = pd.read_csv(pth_clean / f"table5.csv", sep=";", comment="#", index_col=['param', 'coeff'])

tab_a2 = pd.read_csv(pth_clean / f"table_a2.csv", sep=";", comment="#", index_col=['name'])
tab_a3 = pd.read_csv(pth_clean / f"table_a3.csv", sep=";", comment="#", index_col=['name'])

pval = pd.concat([tab4['value'], tab5['value'], tab_a2['value'], tab_a3['value']]).to_dict()

# read data
dfs = []

for mod in ("in", "out", "cloud"):
    df = pd.read_csv(pth_clean / f"fig8_{mod}.csv", sep=";", comment="#", parse_dates=['date'])
    day = df['date'].iloc[0]

    df['hour'] = df['date'].apply(lambda d: (d - day).total_seconds() / 3600)
    df = df.set_index('hour')

    dfs.append((mod, day.dayofyear, df))

# recompute An using formalisms in paper
ca = 37  # [Pa] value around year 2000
na = 2.19  # [g.m-2] from table1 Montpellier around veraison

j_max_25 = app.eq9(na, pval['j_max_25', 'sna'], pval['j_max_25', 'b'])
vc_max_25 = app.eq9(na, pval['vc_max_25', 'sna'], pval['vc_max_25', 'b'])
tpu_25 = app.eq9(na, pval['tpu_25', 'sna'], pval['tpu_25', 'b'])
rd_25 = app.eq9(na, pval['rd_25', 'sna'], pval['rd_25', 'b'])


def alpha(t_leaf):
    return np.interp(t_leaf, [15, 20, 25, 30, 34], [pval['alpha_15_20'],
                                                    pval['alpha_20_25'],
                                                    pval['alpha_25_30'],
                                                    pval['alpha_30_34'],
                                                    pval['alpha_34']])


for mod, doy, df in dfs:
    ans = []
    for hour, row in df.iterrows():
        t_leaf = row['temp']

        # compute temperature dependant params
        j_max = j_max_25 * app.eq8(t_leaf, pval['j_max_c'], pval['j_max_deltaha'],
                                   pval[f'delta_hd'], pval[f'delta_s'])
        vc_max = vc_max_25 * app.eq8(t_leaf, pval['vc_max_c'], pval['vc_max_deltaha'],
                                     pval[f'delta_hd'], pval[f'delta_s'])
        tpu = tpu_25 * app.eq8(t_leaf, pval['tpu_c'], pval['tpu_deltaha'],
                               pval[f'delta_hd'], pval[f'delta_s'])

        gamma_star = app.eq7(t_leaf, pval[f'gamma_star_c'], pval[f'gamma_star_deltaha'])
        gamma_star *= 0.1  # [ppm] to [Pa]

        rd = rd_25 * app.eq7(t_leaf, pval['rd_c'], pval['rd_deltaha'])

        kc = app.eq7(t_leaf, pval['kc_c'], pval['kc_deltaha'])
        kc *= 0.1  # [ppm] to [Pa]
        ko = app.eq7(t_leaf, pval['ko_c'], pval['ko_deltaha'])
        ko *= 0.1  # [mmol.mol-1] to [kPa]

        an, ci, gs = raw.algo(row['ppfd'], row['vpd'], j_max, vc_max, tpu, gamma_star, rd, kc, ko,
                              pval['gb'], alpha(t_leaf), pval['g0'], pval['a1'], pval['d0'],
                              ca, pval['oxygen'])

        ans.append(an)

    df['an_sim'] = ans

# plot data
fig, axes = plt.subplots(4, 3, sharex='all', sharey='row', figsize=(10, 8), squeeze=False)

for j, (mod, doy, df) in enumerate(dfs):
    ax = axes[0, j]
    ax.set_title(f"DOY{doy:d}")
    ax.plot(df.index, df['ppfd'])

    if j == 0:
        ax.set_ylim(-100, 2000)
        ax.set_ylabel("ppfd [µmol photon.m-2.s-1]")

    ax = axes[1, j]
    ax.plot(df.index, df['temp'], '--')

    if j == 0:
        ax.set_ylim(15, 40)
        ax.set_ylabel("Tleaf [°C]")

    ax = ax.twinx()
    ax.plot(df.index, df['vpd'])

    ax.set_ylim(0, 4)
    if j == 2:
        ax.set_ylabel("VPD [kPa]")

    ax = axes[2, j]
    ax.plot(df.index, df['an'], '.')
    ax.plot(df.index, df['an_sim'], '-', color='#000000')

    if j == 0:
        ax.set_ylim(-3, 20)
        ax.set_ylabel("An [µmol CO2.m-2.s-1]")

    ax = axes[3, j]
    ax.plot(df.index, df['transpi'])

    if j == 0:
        ax.set_ylim(-1, 10)
        ax.set_ylabel("E [mmol H20.m-2.s-1]")

for ax in axes[-1, :]:
    ax.set_xlim(0, 24)
    ax.set_xticks(range(0, 25, 5))

fig.tight_layout()
plt.show()
